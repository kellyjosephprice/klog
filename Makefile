IMAGE_NAME = klog
VOLUMES = --volume="$(PWD):/srv/jekyll" \
	--volume=/srv/jekyll/node_modules
JEKYLL_RUN_DEFAULTS = docker run --rm -it \
	$(VOLUMES)

jekyll = docker run --rm -it $(VOLUMES) $(1) $(IMAGE_NAME)

all: serve

image:
	docker build -t $(IMAGE_NAME) .

serve: image
	#xdg-open http://penguin.termina.linux.test:4000
	$(call jekyll, -p 4000:4000)

build: image
	$(call jekyll) bash -c "yarn build && jekyll build"

shell: image
	$(call jekyll, -p 4000:4000) bash

reown:
	sudo chown -R $(shell whoami):$(shell whoami) .
