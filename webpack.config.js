const path = require("path");
const ManifestPlugin = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: "production",
  entry: [
    path.join(__dirname, "webpack", "main"),
    path.join(__dirname, "webpack", "css", "app.scss"),
    path.join(__dirname, "webpack", "css", "resume.scss"),
  ],
  output: {
    filename: "[name].[contenthash].js",
    path: path.resolve(__dirname, "src/assets"),
  },
  module: {
    rules: [
      {
        test: /.js$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
        query: {
          presets: ["@babel/preset-env"],
        },
      }, {
				test: /\.s[ac]ss$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[contenthash].css',
						}
					},
					{
						loader: 'extract-loader'
					},
					{
						loader: 'css-loader?-url'
					},
					{
						loader: 'sass-loader'
					}
				]
			}
    ],
  },
  resolve: {
    extensions: [".json", ".js", ".jsx"],
    modules: ['node_modules', 'webpack'],
  },
  plugins: [
    new ManifestPlugin({
      fileName: './manifest.json',
      writeToFileEmit: true,
    }),
    new CleanWebpackPlugin(),
  ],
};
