require 'byebug'

class Manifest
  def self.path_for(asset)
    manifest[asset]
  end

  def self.manifest
    JSON.parse(File.read('src/assets/manifest.json'))
  rescue
    {}
  end
end

module Jekyll
  class AssetPathTag < Liquid::Tag
    def initialize(tag_name, asset, tokens)
      super
      @full_path = Manifest.path_for(asset.strip)
      puts "#{asset} => #{@full_path}"
    end

    def render(context)
      "assets/#{@full_path}"
    end
  end
end

Liquid::Template.register_tag('asset_path', Jekyll::AssetPathTag)
